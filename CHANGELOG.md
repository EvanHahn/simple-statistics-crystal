# Changelog

## 0.1.0 - 2022-01-30

### Added

- `geometric_mean`
- `linear_regression`
- `mean`
- `median`
- `median_sorted`
- `mode`
- `quantile`
- `quantile_sorted`
- `range`
- `standard_deviation`
- `variance`
