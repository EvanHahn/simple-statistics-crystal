module SimpleStatistics
  # Performs a [simple linear regression](http://en.wikipedia.org/wiki/Simple_linear_regression). Returns a `Line`.
  #
  # If only one coordinate is provided, the resulting `Line` will be flat (i.e., have a `slope` of `0`).
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised. If any of the coordinates don't have a `size` of `2`, a `BadCoordinateError` will be raised.
  #
  # ```
  # line = SimpleStatistics::linear_regression([[0, 0], [1, 1]])
  #
  # line.slope      # => 1
  # line.intercept  # => 0
  # line.at(2)      # => 2
  # ```
  def linear_regression(coordinates : Enumerable(Enumerable(Number))) : Line
    raise Enumerable::EmptyError.new if coordinates.empty?

    coordinate_count = coordinates.size

    if coordinate_count == 1
      coordinate = coordinates.first
      validate_2d_coordinate(coordinate)
      return Line.new(0_f64, coordinate[1].to_f)
    end

    sum_x = 0_f64
    sum_y = 0_f64
    sum_xx = 0_f64
    sum_xy = 0_f64

    coordinates.each do |coordinate|
      validate_2d_coordinate(coordinate)
      x = coordinate[0]
      y = coordinate[1]
      sum_x += x
      sum_y += y
      sum_xx += x * x
      sum_xy += x * y
    end

    slope = (coordinate_count * sum_xy - sum_x * sum_y) / (coordinate_count * sum_xx - sum_x * sum_x)
    intercept = sum_y / coordinate_count - (slope * sum_x) / coordinate_count

    return Line.new(slope, intercept)
  end

  private def validate_2d_coordinate(coordinate : Enumerable(Number)) : Nil
    raise BadCoordinateError.new unless coordinate.size == 2
  end
end
