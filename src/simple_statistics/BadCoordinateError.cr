module SimpleStatistics
  # Raised when a coordinate is invalid.
  class BadCoordinateError < Exception
    # Creates a new `BadCoordinateError`.
    def initialize
      super("Bad coordinate")
    end
  end
end
