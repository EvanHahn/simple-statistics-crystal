module SimpleStatistics
  # Get the [variance](https://en.wikipedia.org/wiki/Variance) of an enumerable.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised.
  #
  # ```
  # SimpleStatistics.variance((1..6))
  # # => 2.91666...
  #
  # SimpleStatistics.variance([])
  # # raises Enumerable::EmptyError
  # ```
  def variance(numbers : Enumerable) : Number
    mean_value = mean(numbers)
    sum_of_deviations_squared = numbers.reduce(0) do |result, n|
      result + (n - mean_value) ** 2
    end
    sum_of_deviations_squared / numbers.size
  end
end
