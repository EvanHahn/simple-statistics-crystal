module SimpleStatistics
  # Get the [quantile](https://en.wikipedia.org/wiki/Quantile) from an enumerable.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised. If the second argument is not in the range `(0..1)`, `SimpleStatistics::BadQuantileError` will be raised.
  #
  # ```
  # SimpleStatistics.quantile([3, 1, 2], 0.5)
  # # => 2
  #
  # SimpleStatistics.quantile([3, 1, 2], 0)
  # # => 1
  #
  # SimpleStatistics.quantile([4, 3], 0.5)
  # # => 3.5
  # ```
  def quantile(numbers : Enumerable, p : Number) : Number
    quantile_sorted(numbers.to_a.sort, p)
  end
end
