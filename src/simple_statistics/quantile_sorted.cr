module SimpleStatistics
  # Get the [quantile](https://en.wikipedia.org/wiki/Quantile) from a sorted enumerable. If you already know that the enumerable is sorted, this is faster than using `quantile`.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised. If the second argument is not in the range `(0..1)`, `SimpleStatistics::BadQuantileError` will be raised.
  #
  # ```
  # SimpleStatistics.quantile_sorted([1, 2, 3], 0.5)
  # # => 2
  #
  # SimpleStatistics.quantile_sorted([1, 2, 3], 0)
  # # => 1
  #
  # SimpleStatistics.quantile_sorted([3, 4], 0.5)
  # # => 3.5
  # ```
  def quantile_sorted(numbers : Enumerable, p : Number) : Number
    raise Enumerable::EmptyError.new if numbers.empty?
    raise BadQuantileError.new unless (0..1).includes? p

    return numbers.last if p == 1
    return numbers.first if p == 0

    size = numbers.size
    ideal_index = size * p
    index = ideal_index.to_i

    if index != ideal_index
      numbers[ideal_index.ceil.to_i - 1]
    elsif size.even?
      (numbers[index - 1] + numbers[index]) / 2
    else
      numbers[index]
    end
  end
end
