module SimpleStatistics
  # Get the [median](https://en.wikipedia.org/wiki/Median) of an enumerable. This is the "middle value".
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised.
  #
  # ```
  # SimpleStatistics.median([3, 1, 2])
  # # => 2
  #
  # SimpleStatistics.median([0, 2, 3, 5])
  # # => 2.5
  # ```
  def median(numbers : Enumerable) : Number
    quantile(numbers, 0.5)
  end
end
