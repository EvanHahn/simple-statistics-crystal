module SimpleStatistics
  # A struct representing a line in 2D space.
  #
  # ```
  # line = SimpleStatistics::Line.new(2, 3)
  #
  # line.slope     # => 2
  # line.intercept # => 3
  #
  # line.at(5) # => 13
  # ```
  struct Line
    # The slope. Aliased as `slope`.
    #
    # ```
    # SimpleStatistics::Line.new(4, 99).m
    # # => 4
    # ```
    getter m

    # The intercept. Aliased as `intercept`.
    #
    # ```
    # SimpleStatistics::Line.new(99, 2).b
    # # => 2
    # ```
    getter b

    # Creates a new `Line` with a slope and intercept.
    #
    # ```
    # line = SimpleStatistics::Line.new(4, 5)
    # line.slope     # => 4
    # line.intercept # => 5
    # ```
    def initialize(@m : Float64, @b : Float64)
    end

    # The slope. Alias of `m`.
    #
    # ```
    # SimpleStatistics::Line.new(4, 99).slope
    # # => 4
    # ```
    def slope
      @m
    end

    # The intercept. Alias of `b`.
    #
    # ```
    # SimpleStatistics::Line.new(99, 2).intercept
    # # => 2
    # ```
    def intercept
      @b
    end

    # The `y` at the given `x`.
    #
    # ```
    # line = SimpleStatistics::Line.new(2, 3)
    # line.at(0)   # => 3
    # line.at(1)   # => 5
    # line.at(1.2) # => 5.4
    # ```
    def at(x : Float64) : Float64
      (@m * x) + @b
    end
  end
end
