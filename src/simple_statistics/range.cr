module SimpleStatistics
  # Get the minimum and maximum of an enumerable as a range. If the enumerable is empty, the range will be infinite and contain all values.
  #
  # ```
  # SimpleStatistics.range([3, 1, 2])
  # # => (1..3)
  #
  # SimpleStatistics.range([5, 5, 5])
  # # => (5..5)
  #
  # SimpleStatistics.range([] of Int32)
  # # => (..)
  # ```
  def range(numbers : Enumerable(T)) : Range forall T
    return (..) if numbers.empty?

    min = numbers.first
    max = numbers.first
    numbers.each do |number|
      if number < min
        min = number
      end
      if number > max
        max = number
      end
    end
    min..max
  end
end
