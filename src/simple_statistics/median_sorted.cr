module SimpleStatistics
  # Get the [median](https://en.wikipedia.org/wiki/Median) of a sorted enumerable. If you already know that the enumerable is sorted, this is faster than using `median`.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised.
  #
  # ```
  # SimpleStatistics.median_sorted([1, 2, 3])
  # # => 2
  #
  # SimpleStatistics.median_sorted([0, 2, 3, 5])
  # # => 2.5
  # ```
  def median_sorted(numbers : Enumerable) : Number
    quantile_sorted(numbers, 0.5)
  end
end
