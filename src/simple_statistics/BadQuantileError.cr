module SimpleStatistics
  # Raised when `quantile` or `quantile_sorted` is passed a bad argument.
  class BadQuantileError < Exception
    # Creates a new `BadQuantileError`.
    def initialize
      super("Bad quantile bounds")
    end
  end
end
