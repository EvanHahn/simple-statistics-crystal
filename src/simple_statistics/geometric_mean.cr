module SimpleStatistics
  # Get the [geometric mean](https://en.wikipedia.org/wiki/Geometric_mean) of an enumerable.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised. If the enumerable contains negative numbers, `SimpleStatistics::InvalidNumberError` will be raised.
  #
  # ```
  # SimpleStatistics.geometric_mean([2, 8])
  # # => 4
  # ```
  def geometric_mean(numbers : Enumerable) : Number
    raise Enumerable::EmptyError.new if numbers.empty?

    product = numbers.reduce(1) do |result, n|
      raise InvalidNumberError.new if n < 0
      result * n
    end

    product ** (1 / numbers.size)
  end
end
