module SimpleStatistics
  # Get the [mode](https://en.wikipedia.org/wiki/Mode_%28statistics%29) of an enumerable, which is the value that appears most frequently. If there is a tie, the most recently-seen mode will be returned.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised.
  #
  # ```
  # SimpleStatistics.mode([5, 4, 4, 3])
  # # => 4
  #
  # SimpleStatistics.mode([5, 4.0, 4, 3])
  # # => 4
  #
  # SimpleStatistics.mode([1, 1, 2, 2, 3])
  # # => 1
  # ```
  def mode(values : Enumerable(T)) forall T
    raise Enumerable::EmptyError.new if values.empty?

    result = values.first
    result_count = 0

    values.each_with_object(Hash(T, Int32).new(0)) do |number, counts|
      count = counts[number] + 1
      if count > result_count
        result = number
        result_count = count
      end
      counts[number] = count
    end

    result
  end
end
