module SimpleStatistics
  # Get the [arithmetic mean](https://en.wikipedia.org/wiki/Arithmetic_mean) (also known as the average) of an enumerable. This is the sum of all of the values divided by the number of values.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised.
  #
  # ```
  # SimpleStatistics.mean([3, 1, 2])
  # # => 2
  #
  # SimpleStatistics.mean([9, 9])
  # # => 9
  #
  # SimpleStatistics.mean([])
  # # raises Enumerable::EmptyError
  # ```
  def mean(numbers : Enumerable) : Number
    raise Enumerable::EmptyError.new if numbers.empty?
    numbers.sum / numbers.size
  end
end
