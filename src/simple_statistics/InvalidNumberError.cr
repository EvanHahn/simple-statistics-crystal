module SimpleStatistics
  # Raised when a number is invalid.
  class InvalidNumberError < Exception
    # Creates a new `InvalidNumberError`.
    def initialize
      super("Invalid number")
    end
  end
end
