module SimpleStatistics
  # Get the [standard deviation](https://en.wikipedia.org/wiki/Standard_deviation) from an enumerable. This is the square root of the `variance`.
  #
  # If the enumerable is empty, `Enumerable::EmptyError` will be raised. If the enumerable only has one entry, `0` will be returned.
  #
  # ```
  # SimpleStatistics.standard_deviation([2, 4, 4, 4, 5, 5, 7, 9])
  # # => 2
  #
  # SimpleStatistics.standard_deviation([99])
  # # => 0
  # ```
  def standard_deviation(numbers : Enumerable) : Number
    Math.sqrt(variance(numbers))
  end
end
