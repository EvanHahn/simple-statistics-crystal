require "./spec_helper"

describe "standard_deviation" do
  # These tests are [lifted from simple-statistics][0] and modified.
  # [0]: https://github.com/simple-statistics/simple-statistics/blob/0e0553cfec91962a9c0ffc7da8f1030c54269bd4/test/standard_deviation.test.js

  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.standard_deviation([] of Int32)
    end
  end

  it "gets the standard deviation of a Wikipedia example" do
    SimpleStatistics.standard_deviation([2, 4, 4, 4, 5, 5, 7, 9]).should eq(2)
  end

  it "gets the standard deviation of 1-3" do
    SimpleStatistics.standard_deviation(1..3).should be_about(0.816)
  end

  it "gets the standard deviation of 0-10" do
    SimpleStatistics.standard_deviation(0..10).should be_about(3.162)
  end

  it "handles the one-element case" do
    SimpleStatistics.standard_deviation([99]).should eq(0)
  end
end
