require "./spec_helper"

describe "median" do
  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.median([] of Int32)
    end
  end

  it "returns the lone number for a repeating enumerable" do
    SimpleStatistics.median([9]).should eq(9)
    SimpleStatistics.median([9, 9]).should eq(9)
    SimpleStatistics.median([9, 9, 9]).should eq(9)
  end

  it "gets proper median of an even-length list" do
    even = [3, 6, 7, 8, 8, 10, 13, 15, 16, 20].shuffle
    SimpleStatistics.median(even).should eq(9)
  end

  it "gets proper median of an odd-length list" do
    odd = [3, 6, 7, 8, 8, 9, 10, 13, 15, 16, 20].shuffle
    SimpleStatistics.median(odd).should eq(9)
  end
end
