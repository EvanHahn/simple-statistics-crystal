require "./spec_helper"

describe "quantile_sorted" do
  # These tests are [lifted from simple-statistics][0] and modified.
  # [0]: https://github.com/simple-statistics/simple-statistics/blob/0e0553cfec91962a9c0ffc7da8f1030c54269bd4/test/quantile.test.js

  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.quantile_sorted([] of Int32, 0.5)
    end
  end

  it "raises SimpleStatistics::BadQuantileError for bad bounds" do
    expect_raises(SimpleStatistics::BadQuantileError) do
      SimpleStatistics.quantile_sorted([1, 2, 3], 1.1)
    end
    expect_raises(SimpleStatistics::BadQuantileError) do
      SimpleStatistics.quantile_sorted([1, 2, 3], -0.5)
    end
  end

  it "gets proper quantiles of an even-length sorted list" do
    even = [3, 6, 7, 8, 8, 10, 13, 15, 16, 20]
    SimpleStatistics.quantile_sorted(even, 0.25).should eq(7)
    SimpleStatistics.quantile_sorted(even, 0.5).should eq(9)
    SimpleStatistics.quantile_sorted(even, 0.75).should eq(15)
  end

  it "gets proper quantiles of an odd-length sorted list" do
    odd = [3, 6, 7, 8, 8, 9, 10, 13, 15, 16, 20]
    SimpleStatistics.quantile_sorted(odd, 0.25).should eq(7)
    SimpleStatistics.quantile_sorted(odd, 0.5).should eq(9)
    SimpleStatistics.quantile_sorted(odd, 0.75).should eq(15)

    SimpleStatistics.quantile_sorted([0, 1, 2, 3, 4], 0.2).should eq(1)
  end

  it "gets the median quantile" do
    [
      [1, 4, 5, 8],
      [2, 4, 4, 5, 8, 10, 50],
    ].each do |list|
      median = SimpleStatistics.median(list)
      SimpleStatistics.quantile_sorted(list, 0.5).should eq(median)
    end
  end

  it "gets the max quantile" do
    SimpleStatistics.quantile_sorted([1, 2, 3], 1).should eq(3)
  end

  it "gets the min quantile" do
    SimpleStatistics.quantile_sorted([1, 2, 3], 0).should eq(1)
  end
end
