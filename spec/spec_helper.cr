require "spec"
require "../src/simple_statistics"

def be_about(number)
  BeAboutExpectation.new(number)
end

struct BeAboutExpectation
  def initialize(@expected_value : Float64)
  end

  def match(actual_value)
    (@expected_value - actual_value).abs < 0.001
  end

  def failure_message(actual_value)
    "Expected: #{actual_value.inspect} to be about #{@expected_value.inspect}"
  end

  def negative_failure_message(actual_value)
    "Expected: #{actual_value.inspect} not to be about #{@expected_value.inspect}"
  end
end
