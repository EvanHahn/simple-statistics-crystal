describe "geometric_mean" do
  # These tests are [lifted from simple-statistics][0] and modified.
  # [0]: https://github.com/simple-statistics/simple-statistics/blob/0e0553cfec91962a9c0ffc7da8f1030c54269bd4/test/geometric_mean.test.js

  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.geometric_mean([] of Int32)
    end
  end

  it "raises SimpleStatistics::InvalidNumberError if any numbers are negative" do
    expect_raises(SimpleStatistics::InvalidNumberError) do
      SimpleStatistics.geometric_mean([-1])
    end
    expect_raises(SimpleStatistics::InvalidNumberError) do
      SimpleStatistics.geometric_mean([5, -1])
    end
  end

  it "gets the geometric mean of two numbers" do
    SimpleStatistics.geometric_mean([2, 8]).should eq(4)
    SimpleStatistics.geometric_mean([4, 1, 1 / 32]).should eq(0.5)
    SimpleStatistics.geometric_mean([2, 32, 1]).should be_about(4.0)
  end

  it "returns 0 if the enumerable contains 0" do
    SimpleStatistics.geometric_mean([0, 1, 2]).should eq(0)
    SimpleStatistics.geometric_mean([2, 0, 1]).should eq(0)
  end
end
