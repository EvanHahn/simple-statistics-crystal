require "./spec_helper"

describe "range" do
  it "returns an infinite Range for empty enumerables" do
    SimpleStatistics.range([] of Int32).should eq(..)
  end

  it "returns a one-number Range for enumerables that are all the same number" do
    SimpleStatistics.range([5]).should eq(5..5)
    SimpleStatistics.range([5, 5, 5]).should eq(5..5)
  end

  it "returns a Range that includes all of the numbers" do
    SimpleStatistics.range([4, -1, 10]).should eq(-1..10)
    SimpleStatistics.range([-1.2, 10.9, 4.2]).should eq(-1.2..10.9)
  end
end
