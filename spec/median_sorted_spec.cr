require "./spec_helper"

describe "median_sorted" do
  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.median_sorted([] of Int32)
    end
  end

  it "returns the lone number for a repeating enumerable" do
    SimpleStatistics.median_sorted([9]).should eq(9)
    SimpleStatistics.median_sorted([9, 9]).should eq(9)
    SimpleStatistics.median_sorted([9, 9, 9]).should eq(9)
  end

  it "gets proper median of an even-length sorted list" do
    even = [3, 6, 7, 8, 8, 10, 13, 15, 16, 20]
    SimpleStatistics.median(even).should eq(9)
  end

  it "gets proper median of an odd-length sorted list" do
    odd = [3, 6, 7, 8, 8, 9, 10, 13, 15, 16, 20]
    SimpleStatistics.median_sorted(odd).should eq(9)
  end
end
