require "./spec_helper"

describe "mean" do
  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.mean([] of Int32)
    end
  end

  it "returns the arithmetic mean" do
    SimpleStatistics.mean([5]).should eq(5)
    SimpleStatistics.mean([6.9]).should eq(6.9)

    SimpleStatistics.mean(6..9).should eq(7.5)
    SimpleStatistics.mean([1.2, 3.4, 5.6]).should eq(3.4)
  end
end
