require "./spec_helper"

describe "variance" do
  # These tests are [lifted from simple-statistics][0] and modified.
  # [0]: https://github.com/simple-statistics/simple-statistics/blob/0e0553cfec91962a9c0ffc7da8f1030c54269bd4/test/variance.test.js

  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.variance([] of Int32)
    end
  end

  it "gets the variance of 1-6" do
    SimpleStatistics.variance((1..6)).should eq(35 / 12)
  end

  it "handles the one-element corner case" do
    SimpleStatistics.variance([99]).should eq(0)
  end
end
