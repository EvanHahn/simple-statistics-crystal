require "./spec_helper"

describe "mode" do
  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.mode([] of Int32)
    end
  end

  it "returns the most common value, normalizing different number types" do
    SimpleStatistics.mode([5]).should eq(5)
    SimpleStatistics.mode([5, 5, 5]).should eq(5)
    SimpleStatistics.mode([5, 5.0, 5]).should eq(5)

    SimpleStatistics.mode([1, 1, 2]).should eq(1)
    SimpleStatistics.mode([1.0, 1, 2]).should eq(1)
    SimpleStatistics.mode([1, 1, 2, 3]).should eq(1)

    SimpleStatistics.mode([1, 1, 2, 3, 3]).should eq(1)
    SimpleStatistics.mode([1, 1.0, 2, 3, 3]).should eq(1)
    SimpleStatistics.mode([1, 1, 2, 3, 3.0]).should eq(1)
    SimpleStatistics.mode([1, 1.0, 2, 3, 3.0]).should eq(1)

    SimpleStatistics.mode([1, 2, 2, 3, 3, 4, 1, 4, 1]).should eq(1)
    SimpleStatistics.mode([1, 1.1, 4, 1.1, 3]).should eq(1.1)

    SimpleStatistics.mode([1, 2, 3, 4, 5]).should eq(1)
  end
end
