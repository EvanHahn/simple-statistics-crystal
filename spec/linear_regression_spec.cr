require "./spec_helper"

describe "linear_regression" do
  # These tests are [lifted from simple-statistics][0] and modified.
  # [0]: https://github.com/simple-statistics/simple-statistics/blob/0e0553cfec91962a9c0ffc7da8f1030c54269bd4/test/linear_regression.test.js

  it "raises Enumerable::EmptyError for empty enumerables" do
    expect_raises(Enumerable::EmptyError) do
      SimpleStatistics.linear_regression([] of Array(Int32))
    end
  end

  it "raises SimpleStatistics::BadCoordinateError for invalid coordinates" do
    expect_raises(SimpleStatistics::BadCoordinateError) do
      SimpleStatistics.linear_regression([[0, 0], [1]])
    end
    expect_raises(SimpleStatistics::BadCoordinateError) do
      SimpleStatistics.linear_regression([[0, 0], [] of Int32])
    end
    expect_raises(SimpleStatistics::BadCoordinateError) do
      SimpleStatistics.linear_regression([[0, 0], [1, 2, 3]])
    end
  end

  it "returns a Line" do
    line = SimpleStatistics.linear_regression([[0, 0], [1, 1]])
    line.should be_a(SimpleStatistics::Line)
  end

  it "correctly generates a line for a 0, 0 to 1, 1 dataset" do
    line = SimpleStatistics.linear_regression([[0, 0], [1, 1]])
    line.slope.should eq(1)
    line.intercept.should eq(0)
  end

  it "correctly generates a line for a 0, 0 to 1, 0 dataset" do
    line = SimpleStatistics.linear_regression([[0, 0], [1, 0]])
    line.slope.should eq(0)
    line.intercept.should eq(0)
  end

  it "handles a single-point sample" do
    line = SimpleStatistics.linear_regression([[3, 5]])
    line.slope.should eq(0)
    line.intercept.should eq(5)
  end

  it "handles a line at 50% grade" do
    line = SimpleStatistics.linear_regression([[0, 0], [1, 0.5]])
    line.slope.should eq(0.5)
    line.intercept.should eq(0)
  end

  it "handles a line with a high y-intercept" do
    line = SimpleStatistics.linear_regression([[0, 20], [1, 10]])
    line.slope.should eq(-10)
    line.intercept.should eq(20)
  end
end
