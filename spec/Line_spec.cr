require "./spec_helper"

describe SimpleStatistics::Line do
  describe "m" do
    it "is the constructed value" do
      SimpleStatistics::Line.new(5, 6).m.should eq(5)
    end

    it "is aliased as `slope`" do
      SimpleStatistics::Line.new(5, 6).slope.should eq(5)
    end
  end

  describe "b" do
    it "is the constructed value" do
      SimpleStatistics::Line.new(5, 6).b.should eq(6)
    end

    it "is aliased as `intercept`" do
      SimpleStatistics::Line.new(5, 6).intercept.should eq(6)
    end
  end

  describe "at" do
    it "returns the Y value at the given X" do
      line = SimpleStatistics::Line.new(2.1, 3.2)
      line.at(-1).should eq(1.1)
      line.at(0).should eq(3.2)
      line.at(0.5).should eq(4.25)
      line.at(8).should eq(20)
    end
  end
end
