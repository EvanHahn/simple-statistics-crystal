# Simple Statistics

A [Crystal](https://crystal-lang.org/) shard for doing simple statistical operations, such as `median` or `standard_deviation`. Inspired by [the JavaScript version](https://github.com/simple-statistics/simple-statistics).

## Installation

Add the dependency to your `shard.yml`...

```yaml
dependencies:
  simple_statistics:
    gitlab: EvanHahn/simple-statistics.cr
```

...and run `shards install`.

## Usage

```crystal
require "simple_statistics"

SimpleStatistics.mean([4, 2, 0])
# => 6

SimpleStatistics.median([9, 0, 2, 1, 0])
# => 1

SimpleStatistics.standard_deviation(0..10)
# => 3.162...
```

For more, refer to [the API docs](https://evanhahn.gitlab.io/simple-statistics-crystal/SimpleStatistics.html) or [the source code](https://gitlab.com/EvanHahn/simple-statistics-crystal/).
